# encoding: utf-8

class Livro < Midia
  attr_accessor :autor
  attr_accessor :categoria
  attr_accessor :isbn

  include FormatadorMoeda

  FormatadorMoeda::formata_moeda :preco, :preco_com_desconto

  def initialize(titulo, autor, isbn="xxx", num_pag, 
                preco, categoria)
    @titulo = titulo
    @autor = autor
    @num_pag = num_pag
    @isbn = isbn
    @preco = preco
    @categoria = categoria
    @desconto = 0.15
  end

  def to_s
    "Autor: #{@autor}, ISBN: #{@isbn}, Pag: #{@num_pag}, #{@preco}, #{@categoria}"
  end

  def eql?(outro_livro)
    @isbn == outro_livro.isbn
  end

  def hash
    @isbn.hash
  end


end
