module FormatadorMoeda

  def self.formata_moeda(*variaveis_e_metodos)
    variaveis_e_metodos.each do |name|
      define_method("#{name}_formatado") do
        preco = respond_to?(name) ? send(name) : instance_variable_get("@#{name}")
        "R$ #{preco}"
      end
    end
  end

end
